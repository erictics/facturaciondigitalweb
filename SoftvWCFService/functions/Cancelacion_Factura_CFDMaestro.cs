﻿using MizarCFD.DAL;
using softv.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SoftvWCFService.functions
{
    

    public class Cancelacion_Factura_CFDMaestro
    {
        bool TimbrePrueba = bool.Parse(ConfigurationManager.AppSettings["TimbrePrueba"].ToString());

      


        Graba_Factura_DigitalMaestro Graba_Factura_DigitalMaestro = new Graba_Factura_DigitalMaestro();
        Graba_Factura_DigitalPago Graba_Factura_DigitalPago = new Graba_Factura_DigitalPago();
        public string locID_Sucursal_Mizart = "";
        public string locID_Compania_Mizart = "";
        string ConnectionString = ConfigurationManager.ConnectionStrings["NEWSOFTV"].ConnectionString;


        public ResultEntity GetCancelacion_Factura_CFDMaestro(long? oClv_FacturaCFD, string tipo)
        {
            ResultEntity result = new ResultEntity();
            try
            {
                locID_Sucursal_Mizart = "";
                locID_Compania_Mizart = "";
                long? ClvFacturaCfd = 0;
                if (tipo == "M")
                {
                    Dime_Aque_Compania_FacturarleMaestrotvzac(oClv_FacturaCFD);
                    
                       ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_FacturaCFD, "M").Value;
                }
                else if (tipo == "P")
                {
                    Dime_Aque_Compania_FacturarlePago(oClv_FacturaCFD);
                   ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_FacturaCFD, "P").Value;
                }
                else if (tipo == "N" || tipo == "C")//o C
                {
                    Dime_Aque_Compania_Facturarle(oClv_FacturaCFD);
                    ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_FacturaCFD, "N").Value;
                }
                else if (tipo == "T")
                {
                    Dime_Aque_Compania_FacturarleNOTAMaestro(oClv_FacturaCFD);
                    ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_FacturaCFD, "T").Value;
                }
                else if (tipo == "G")
                {
                    Dime_Aque_Compania_FacturarleGlobal(oClv_FacturaCFD);
                    ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_FacturaCFD, "G").Value;
                }
                else if (tipo == "O")
                {
                    Dime_Aque_Compania_FacturarleNOTA(oClv_FacturaCFD);
                    ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_FacturaCFD, "O").Value;
                    //DBFuncion db = new DBFuncion();
                    //db.agregarParametro("@Clv_NotaDeCredito", SqlDbType.BigInt ,oClv_FacturaCFD);
                    //db.consultaSinRetorno("CancelaNotaCredito");
                }

                if (ClvFacturaCfd > 0)
                {
                    List<String> listComprobantes = new List<String>();
                    listComprobantes.Add(ClvFacturaCfd.ToString());
                    MizarCFD.BRL.CFD oCFD = new MizarCFD.BRL.CFD();
                    DAConexion Cnx = new DAConexion("HL", "sa", "sa");
                    oCFD.Inicializar();
                    oCFD.IdCFD = ClvFacturaCfd.ToString();
                    oCFD.MotivoCancelacion = "CANCELACION";
                    oCFD.FechaCancelacion = DateTime.Now;
                    oCFD.EstaCancelado = "1";

                    /*if (!oCFD.CancelarCFDi(Cnx, locID_Compania_Mizart, "CANCELACION", listComprobantes.ToArray(), TimbrePrueba) == false)
                    {
                        foreach (MizarCFD.DAL.DAMensajesSistema.RegistroMensaje oMensaje in oCFD.MensajesSistema.ListaMensajes)
                        {
                            result.IdResult = -1;
                            result.Message = oMensaje.TextoMensaje + " " + oMensaje.Contexto;
                            return result;
                        }

                    }

                    int MiError = 0;
                    if (!oCFD.ModificarEstatusCancelado(Cnx) == false)
                    {
                        MiError = 1;
                        foreach (MizarCFD.DAL.DAMensajesSistema.RegistroMensaje oMensaje in oCFD.MensajesSistema.ListaMensajes)
                        {
                            result.IdResult = -1;
                            result.Message = oMensaje.TextoMensaje + " " + oMensaje.Contexto;
                            return result;
                        }
                    }

                    UPS_Modificar_Rel_FacturasCFD(oClv_FacturaCFD);

                    result.IdResult = 1;
                    result.Message = "La factura se canceló correctamente";*/

                    oCFD.CancelarNuevoCFDi(Cnx, locID_Compania_Mizart, "CANCELACION", listComprobantes.ToArray(), TimbrePrueba);

                    foreach (MizarCFD.DAL.DAMensajesSistema.RegistroMensaje oMensaje in oCFD.MensajesSistema.ListaMensajes)
                    {
                        result.IdResult = -1;
                        result.Message = "";

                        result.Message = oMensaje.TextoMensaje;

                        DBFuncion db = new DBFuncion();
                        db.agregarParametro("@Clv_Factura", SqlDbType.BigInt, oClv_FacturaCFD);
                        db.agregarParametro("@Tipo", SqlDbType.VarChar, tipo);
                        db.agregarParametro("@MensajeRespuesta", SqlDbType.VarChar, oMensaje.TextoMensaje);
                        db.agregarParametro("@MensajeUsuario", SqlDbType.VarChar, result.Message);
                        db.consultaSinRetorno("GuardaMensajeCancelacion");

                        //if (oMensaje.TextoMensaje.Contains(" 201 ") || oMensaje.TextoMensaje.Contains(" 202 ") || oMensaje.TextoMensaje.Contains("Cancelable sin aceptación"))
                        //{
                        result.IdResult = 1;
                        //}
                        result.Message = "<p>" + result.Message.Replace("\n", "<br>") + "</p>";
                    }

                }
                else
                {
                    result.IdResult = 0;
                    result.Message = "El ticket no tiene factura fiscal";
                }
                return result;

            }
            catch (Exception ex)
            {
                result.IdResult = -1;
                result.Message = ex.InnerException.ToString();
                return result;
            }
        }

        public void Dime_Aque_Compania_FacturarleNOTA(long? oClv_NOTA)
        {
            locID_Compania_Mizart = "";
            locID_Sucursal_Mizart = "";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("Dime_Aque_Compania_FacturarleNOTA");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;

                    SqlParameter prm1 = new SqlParameter("@Clv_NOTA", SqlDbType.BigInt);
                    prm1.Direction = ParameterDirection.Input;
                    prm1.Value = oClv_NOTA;
                    COMANDO.Parameters.Add(prm1);

                    SqlParameter prm2 = new SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50);
                    prm2.Direction = ParameterDirection.Output;
                    prm2.Value = 0;
                    COMANDO.Parameters.Add(prm2);

                    SqlParameter prm3 = new SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50);
                    prm3.Direction = ParameterDirection.Output;
                    prm3.Value = "";
                    COMANDO.Parameters.Add(prm3);
                    COMANDO.ExecuteNonQuery();
                    locID_Compania_Mizart = prm2.Value.ToString();
                    locID_Sucursal_Mizart = prm3.Value.ToString();
                }

                catch (Exception ex)
                {
                    // log.Llenalog(ex.Source.ToString() + " " + ex.Message);
                    throw new Exception("Error Dime_Aque_Compania_FacturarleNOTA" + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }
            }
        }

        public void Dime_Aque_Compania_FacturarleGlobal(long? oClv_Factura)
        {
            locID_Compania_Mizart = "";
            locID_Sucursal_Mizart = "";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("Dime_Aque_Compania_FacturarleGlobal");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;

                    SqlParameter prm1 = new SqlParameter("@Clv_Facturaglobal", SqlDbType.BigInt);
                    prm1.Direction = ParameterDirection.Input;
                    prm1.Value = oClv_Factura;
                    COMANDO.Parameters.Add(prm1);

                    SqlParameter prm2 = new SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50);
                    prm2.Direction = ParameterDirection.Output;
                    prm2.Value = 0;
                    COMANDO.Parameters.Add(prm2);

                    SqlParameter prm3 = new SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50);
                    prm3.Direction = ParameterDirection.Output;
                    prm3.Value = "";
                    COMANDO.Parameters.Add(prm3);
                    COMANDO.ExecuteNonQuery();
                    locID_Compania_Mizart = prm2.Value.ToString();
                    locID_Sucursal_Mizart = prm3.Value.ToString();
                }

                catch (Exception ex)
                {
                    //log.Llenalog(ex.Source.ToString() + " " + ex.Message);
                    throw new Exception("Error Dime_Aque_Compania_FacturarleGlobal" + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }
            }
        }

        public void Dime_Aque_Compania_FacturarleNOTAMaestro(long? oClv_NOTA)
        {
            locID_Compania_Mizart = "";
            locID_Sucursal_Mizart = "";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("Dime_Aque_Compania_FacturarleNOTAMaestro");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;

                    SqlParameter prm1 = new SqlParameter("@Clv_NOTA", SqlDbType.BigInt);
                    prm1.Direction = ParameterDirection.Input;
                    prm1.Value = oClv_NOTA;
                    COMANDO.Parameters.Add(prm1);

                    SqlParameter prm2 = new SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50);
                    prm2.Direction = ParameterDirection.Output;
                    prm2.Value = 0;
                    COMANDO.Parameters.Add(prm2);

                    SqlParameter prm3 = new SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50);
                    prm3.Direction = ParameterDirection.Output;
                    prm3.Value = "";
                    COMANDO.Parameters.Add(prm3);
                    COMANDO.ExecuteNonQuery();
                    locID_Compania_Mizart = prm2.Value.ToString();
                    locID_Sucursal_Mizart = prm3.Value.ToString();
                }

                catch (Exception ex)
                {
                    // log.Llenalog(ex.Source.ToString() + " " + ex.Message);
                    throw new Exception("Error Dime_Aque_Compania_FacturarleNOTAMaestro" + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }
            }
        }

        public void Dime_Aque_Compania_Facturarle(long? oClv_Factura)
        {
            locID_Compania_Mizart = "";
            locID_Sucursal_Mizart = "";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("Dime_Aque_Compania_Facturarle");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;

                    SqlParameter prm1 = new SqlParameter("@Clv_Factura", SqlDbType.BigInt);
                    prm1.Direction = ParameterDirection.Input;
                    prm1.Value = oClv_Factura;
                    COMANDO.Parameters.Add(prm1);

                    SqlParameter prm2 = new SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50);
                    prm2.Direction = ParameterDirection.Output;
                    prm2.Value = 0;
                    COMANDO.Parameters.Add(prm2);

                    SqlParameter prm3 = new SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50);
                    prm3.Direction = ParameterDirection.Output;
                    prm3.Value = "";
                    COMANDO.Parameters.Add(prm3);
                    COMANDO.ExecuteNonQuery();
                    locID_Compania_Mizart = prm2.Value.ToString();
                    locID_Sucursal_Mizart = prm3.Value.ToString();
                }

                catch (Exception ex)
                {
                    //log.Llenalog(ex.Source.ToString() + " " + ex.Message);
                    throw new Exception("Error Dime_Aque_Compania_Facturarle" + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }
            }
        }

        public void Dime_Aque_Compania_FacturarleMaestrotvzac(long? oClv_Factura)
        {
            locID_Compania_Mizart = "";
            locID_Sucursal_Mizart = "";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("Dime_Aque_Compania_FacturarleMaestro");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;

                    SqlParameter prm1 = new SqlParameter("@Clv_FacturaMaestro", SqlDbType.BigInt);
                    prm1.Direction = ParameterDirection.Input;
                    prm1.Value = oClv_Factura;
                    COMANDO.Parameters.Add(prm1);

                    SqlParameter prm2 = new SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50);
                    prm2.Direction = ParameterDirection.Output;
                    prm2.Value = 0;
                    COMANDO.Parameters.Add(prm2);

                    SqlParameter prm3 = new SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50);
                    prm3.Direction = ParameterDirection.Output;
                    prm3.Value = "";
                    COMANDO.Parameters.Add(prm3);
                    COMANDO.ExecuteNonQuery();
                    locID_Compania_Mizart = prm2.Value.ToString();
                    locID_Sucursal_Mizart = prm3.Value.ToString();
                }

                catch (Exception ex)
                {
                   
                    throw new Exception("Error Dime_Aque_Compania_FacturarleMaestro" + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }
            }
        }


        public void Dime_Aque_Compania_FacturarlePago(long? oClv_Factura)
        {
            locID_Compania_Mizart = "";
            locID_Sucursal_Mizart = "";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("Dime_Aque_Compania_FacturarlePago");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;

                    SqlParameter prm1 = new SqlParameter("@Clv_Pago", SqlDbType.BigInt);
                    prm1.Direction = ParameterDirection.Input;
                    prm1.Value = oClv_Factura;
                    COMANDO.Parameters.Add(prm1);

                    SqlParameter prm2 = new SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50);
                    prm2.Direction = ParameterDirection.Output;
                    prm2.Value = 0;
                    COMANDO.Parameters.Add(prm2);

                    SqlParameter prm3 = new SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50);
                    prm3.Direction = ParameterDirection.Output;
                    prm3.Value = "";
                    COMANDO.Parameters.Add(prm3);
                    COMANDO.ExecuteNonQuery();
                    locID_Compania_Mizart = prm2.Value.ToString();
                    locID_Sucursal_Mizart = prm3.Value.ToString();
                }

                catch (Exception ex)
                {
                    // log.Llenalog(ex.Source.ToString() + " " + ex.Message);
                    throw new Exception("Error Dime_Aque_Compania_FacturarleMaestro" + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }
            }
        }


        public void UPS_Modificar_Rel_FacturasCFD(long? oClv_FacturaCFD)
        {

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("UPS_Modificar_Rel_FacturasCFD");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;

                    SqlParameter prm1 = new SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt);
                    prm1.Direction = ParameterDirection.Input;
                    prm1.Value = oClv_FacturaCFD;
                    COMANDO.Parameters.Add(prm1);

                    COMANDO.ExecuteNonQuery();

                }

                catch (Exception ex)
                {
                    // log.Llenalog(ex.Source.ToString() + " " + ex.Message);
                    throw new Exception("Error UPS_Modificar_Rel_FacturasCFD" + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }

            }
        }


        public long? SP_DIMESIEXISTEFACTURADIGITAL(long? oClv_Factura, string oTipoFact)
        {

            long result = 0;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("SP_DIMESIEXISTEFACTURADIGITAL");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;


                    SqlParameter parametro = new SqlParameter("@Clv_Factura", SqlDbType.BigInt);
                    parametro.Direction = ParameterDirection.Input;
                    parametro.Value = oClv_Factura;
                    COMANDO.Parameters.Add(parametro);

                    SqlParameter parametro1 = new SqlParameter("@TipoFact", SqlDbType.VarChar, 10);
                    parametro1.Direction = ParameterDirection.Input;
                    parametro1.Value = oTipoFact;
                    COMANDO.Parameters.Add(parametro1);

                    SqlParameter parametro2 = new SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt);
                    parametro2.Direction = ParameterDirection.Output;
                    parametro2.Value = 0;
                    COMANDO.Parameters.Add(parametro2);

                    COMANDO.ExecuteNonQuery();
                    result = long.Parse(parametro2.Value.ToString());
                }
                catch (Exception ex)
                {
                    throw new Exception("SP_DIMESIEXISTEFACTURADIGITAL " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }
                return result;

            }

        }



    }
}