﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SoftvWCFService.functions
{
    public class Log
    {

        public void Llenalog(string texto)
        {
            try
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string te= DateTime.Now + " - " + texto;
                string path = "log" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + ".txt";

                if (!File.Exists(path))
                {
                  
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine(te);
                     
                    }
                }

            }
            catch
            {

            }

        }
    }
}