﻿using MizarCFD.BRL;
using MizarCFD.DAL;
using softv.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace SoftvWCFService.functions
{
    public class Graba_Factura_Nota
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["NEWSOFTV"].ConnectionString;
        Graba_Factura_Digital grabaFacMaes = new Graba_Factura_Digital();
        public string locID_Sucursal_Mizart = "";
        public string locID_Compania_Mizart = "";
        public long GloClv_FacturaCFD = 0;
        public string GloFORMADEPAGO = "";
        public string GloCONDICIONESDEPAGO = "";
        public string GloMOTIVODESCUENTO = "";
        public string GloMETODODEPAGO = "";
        public string GloTIPODECOMPROBANTE = "";
        public string GloPAGOENPARCIALIDADES = "";
        public string GloLUGAREXPEDICION = "";
        public string GloREGIMEN = "";
        public string GloMONEDA = "";
        public string GloVERSON = "";
        public string GloTIPOFACTOR = "";
        public string GloFORMAPAGO = "";
        public string GloTIPOCAMBIO = "";
        public string GloUSOCFD = "";
        public string GloFECHAVIGENCIA = "";
        public string GloUUIDRel = "";
        public string GlonumCtaPago = "";
        public bool SumaSubtotalYIeps = true;
        bool TimbrePrueba = bool.Parse(ConfigurationManager.AppSettings["TimbrePrueba"].ToString());
        public ResultEntity GetGraba_Factura_Nota(long? oClv_Nota, long? oIden)
        {
            ResultEntity result = new ResultEntity();
            oIden = BusFacFiscalOledbNota(oClv_Nota);
            EnviarFactura EnviarFactura = new EnviarFactura();
            if (oIden > 0)
            {

                Dime_Aque_Compania_FacturarleNOTA(oClv_Nota);
                long? ClvFacturaCfd = 0;
                ClvFacturaCfd = grabaFacMaes.SP_DIMESIEXISTEFACTURADIGITAL(oClv_Nota, "O").Value;


                if (ClvFacturaCfd == 0)
                {

                    string oTotalConPuntos = "";
                    string oSubTotal = "";
                    string oTotalSinPuntos = "";
                    string oDescuento = "";
                    string oiva = "";
                    string oieps = "";
                    string oTasaIva = "";
                    string oTasaIeps = "";
                    string oFecha = "";
                    string oTotalImpuestos = "";

                    #region DameFacDig_Parte_1Nota
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();

                            SqlCommand COMANDO = new SqlCommand("DameFacDig_Parte_1Nota", connection);
                            COMANDO.CommandType = CommandType.StoredProcedure;
                            COMANDO.CommandTimeout = 0;

                            SqlParameter parametro = new SqlParameter("@Clv_Nota", SqlDbType.BigInt);
                            parametro.Direction = ParameterDirection.Input;
                            parametro.Value = oClv_Nota;
                            COMANDO.Parameters.Add(parametro);

                            SqlParameter parametro1 = new SqlParameter("@TotalConPuntos", SqlDbType.Money);
                            parametro1.Direction = ParameterDirection.Output;
                            parametro1.Value = 0;
                            COMANDO.Parameters.Add(parametro1);

                            SqlParameter parametro2 = new SqlParameter("@SubTotal", SqlDbType.Money);
                            parametro2.Direction = ParameterDirection.Output;
                            parametro2.Value = 0;
                            COMANDO.Parameters.Add(parametro2);

                            SqlParameter parametro3 = new SqlParameter("@TotalSinPuntos", SqlDbType.Money);
                            parametro3.Direction = ParameterDirection.Output;
                            parametro3.Value = 0;
                            COMANDO.Parameters.Add(parametro3);

                            SqlParameter parametro4 = new SqlParameter("@Descuento", SqlDbType.Money);
                            parametro4.Direction = ParameterDirection.Output;
                            parametro4.Value = 0;
                            COMANDO.Parameters.Add(parametro4);

                            SqlParameter parametro5 = new SqlParameter("@iva", SqlDbType.Money);
                            parametro5.Direction = ParameterDirection.Output;
                            parametro5.Value = 0;
                            COMANDO.Parameters.Add(parametro5);

                            SqlParameter parametro6 = new SqlParameter("@ieps", SqlDbType.Money);
                            parametro6.Direction = ParameterDirection.Output;
                            parametro6.Value = 0;
                            COMANDO.Parameters.Add(parametro6);

                            SqlParameter parametro7 = new SqlParameter("@TasaIva", SqlDbType.Money);
                            parametro7.Direction = ParameterDirection.Output;
                            parametro7.Value = 0;
                            COMANDO.Parameters.Add(parametro7);

                            SqlParameter parametro8 = new SqlParameter("@TasaIeps", SqlDbType.Money);
                            parametro8.Direction = ParameterDirection.Output;
                            parametro8.Value = 0;
                            COMANDO.Parameters.Add(parametro8);

                            SqlParameter parametro9 = new SqlParameter("@Fecha", SqlDbType.DateTime);
                            parametro9.Direction = ParameterDirection.Output;
                            parametro9.Value = 0;
                            COMANDO.Parameters.Add(parametro9);

                            SqlParameter parametro10 = new SqlParameter("@TotalImpuestos", SqlDbType.Money);
                            parametro10.Direction = ParameterDirection.Output;
                            parametro10.Value = 0;
                            COMANDO.Parameters.Add(parametro10);


                            COMANDO.ExecuteNonQuery();
                            oTotalConPuntos = decimal.Parse(parametro1.Value.ToString()).ToString("0.00");
                            oSubTotal = decimal.Parse(parametro2.Value.ToString()).ToString("0.00");
                            oTotalSinPuntos = decimal.Parse(parametro3.Value.ToString()).ToString("0.00");
                            oDescuento = decimal.Parse(parametro4.Value.ToString()).ToString("0.00");
                            oiva = decimal.Parse(parametro5.Value.ToString()).ToString("0.00");
                            oieps = decimal.Parse(parametro6.Value.ToString()).ToString("0.00");
                            oTasaIva = decimal.Parse(parametro7.Value.ToString()).ToString("0.00");
                            oTasaIeps = decimal.Parse(parametro8.Value.ToString()).ToString("0.00");
                            oFecha = parametro9.Value.ToString();
                            oTotalImpuestos = decimal.Parse(parametro10.Value.ToString()).ToString("0.00");

                        }

                        catch (Exception ex)
                        {
                            throw new Exception("Error getting data DameFacDig_Parte_1Nota " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();

                        }

                    }
                    #endregion

                    Usp_Ed_DameDatosFacDigNota(oClv_Nota.Value, long.Parse(locID_Compania_Mizart));

                    DAConexion Cnx = new DAConexion("HL", "sa", "sa");
                    MizarCFD.BRL.CFD oCFD = new MizarCFD.BRL.CFD();
                    long? oId_AsociadoLlave = 0;
                    oId_AsociadoLlave = oIden;
                    oCFD = new MizarCFD.BRL.CFD();
                    oCFD.Inicializar();
                    oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision;

                    if (locID_Sucursal_Mizart.Length > 0 && locID_Sucursal_Mizart != "0")
                    {
                        oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart);
                    }
                    else
                    {
                        oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart);
                    }
                    oCFD.PrepararDatosReceptorPorId(Cnx, oId_AsociadoLlave.ToString(), "1", "0");

                    oCFD.EsNotaCredito = "1";
                    oCFD.EsTimbrePrueba = TimbrePrueba;
                    oCFD.Timbrador = CFD.PAC.Mizar;
                    oCFD.Version = GloVERSON;
                    oCFD.IdMoneda = "1";
                    oCFD.FormaDePago = GloFORMADEPAGO;
                    oCFD.CondicionesDePago = GloCONDICIONESDEPAGO;
                    oCFD.SubTotal = oSubTotal;
                    oCFD.Descuento = oDescuento;
                    oCFD.MotivoDescuento = GloMOTIVODESCUENTO;
                    oCFD.Total = oTotalSinPuntos;
                    oCFD.MetodoDePago = GloMETODODEPAGO;
                    oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE;
                    oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE;
                    oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES;
                    oCFD.LugarExpedicion = GloPAGOENPARCIALIDADES;
                    oCFD.CuentaPago = GlonumCtaPago;


                    //'Parte especial de las notas
                    oCFD.Moneda = GloMONEDA;
                    oCFD.UsoCFDI = GloUSOCFD;
                    oCFD.TipoRelacion = "01";
                    oCFD.UUIDRelacionados = GloUUIDRel;
                    oCFD.UUIDRel.Add(GloUUIDRel);
                    oCFD.TipoCambio = GloTIPOCAMBIO;

                    MizarCFD.BRL.CFDRegimenesFiscales oRegimen = new MizarCFD.BRL.CFDRegimenesFiscales();
                    oRegimen.IdRegimen = "0";
                    oRegimen.IdCFD = "0";
                    oRegimen.Regimen = GloREGIMEN;
                    oCFD.RegimenesFiscales.Add(oRegimen);
                    oCFD.Impuestos.TotalImpuestosRetenidos = "0";
                    oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos;

                    CFDImpuestosTraslados oImpuestosTraslados = new CFDImpuestosTraslados();



                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        IDataReader rd = null;
                        try
                        {

                            if (connection.State == ConnectionState.Closed)
                                connection.Open();

                            SqlCommand COMANDO = new SqlCommand("exec DameFacDig_Parte_2Nota " + oClv_Nota);
                            COMANDO.CommandTimeout = 0;
                            COMANDO.Connection = connection;
                            CFDConceptos oConcepto;
                            int oContrador = 1;

                            rd = COMANDO.ExecuteReader();
                            while (rd.Read())
                            {

                                oConcepto = new CFDConceptos();
                                oConcepto.Cantidad = rd[0].ToString();
                                //   oConcepto.UnidadMedida = "No Aplica";
                                oConcepto.NumeroIdentificacion = rd[1].ToString();
                                oConcepto.Descripcion = rd[2].ToString();
                                oConcepto.ValorUnitario = decimal.Parse(rd[3].ToString()).ToString("0.00");
                                oConcepto.Importe = decimal.Parse(rd[4].ToString()).ToString("0.00");
                                oConcepto.TrasladarIVA = "1";

                                oConcepto.Descuento = "0";
                                oConcepto.idClaveProdServ = rd[5].ToString();
                                oConcepto.TipoFactor = rd[6].ToString();
                                oConcepto.TasaIVA = decimal.Parse(rd[7].ToString()).ToString("0.00");
                                oConcepto.IVATRAS_Importe = decimal.Parse(rd[8].ToString()).ToString("0.00");

                                if (double.Parse(rd[11].ToString()) > 0)
                                {
                                    oConcepto.BaseIEPS = decimal.Parse(rd[4].ToString()).ToString("0.00");
                                    oConcepto.BaseIVA = (SumaSubtotalYIeps == true) ? decimal.Parse((decimal.Parse(rd[4].ToString()) + decimal.Parse(rd[11].ToString())).ToString()).ToString("0.00") : decimal.Parse(decimal.Parse(rd[4].ToString()).ToString()).ToString("0.00");
                                    oConcepto.IEPSTRAS_Importe = decimal.Parse(rd[11].ToString()).ToString("0.00");
                                    oConcepto.TasaIEPS = decimal.Parse(rd[10].ToString()).ToString("0.00");
                                    oConcepto.FactorIEPS = rd[9].ToString();
                                    oConcepto.TrasladarIEPS = "1";
                                }
                                else
                                {
                                    oConcepto.BaseIEPS = "0";
                                    oConcepto.BaseIVA = decimal.Parse(rd[4].ToString()).ToString("0.00");
                                    oConcepto.IEPSTRAS_Importe = "0";
                                    oConcepto.TrasladarIEPS = "0";
                                }

                                oConcepto.idUnidadMedida = rd[13].ToString();
                                oConcepto.UnidadMedida = rd[12].ToString();
                                oConcepto.IVARET_Importe = "0";
                                oConcepto.ISRRET_Importe = "0";

                                if (double.Parse(rd[8].ToString()) > 0)
                                {
                                    oImpuestosTraslados = new CFDImpuestosTraslados();
                                    oImpuestosTraslados.IdRegistroTraslado = oContrador.ToString();
                                    oImpuestosTraslados.Impuesto = "IVA";
                                    oImpuestosTraslados.Importe = decimal.Parse(rd[8].ToString()).ToString("0.00");
                                    oImpuestosTraslados.Tasa = decimal.Parse(rd[7].ToString()).ToString("0.00");
                                    oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR;
                                    oImpuestosTraslados.IEPSTRAS_Importe = "0";
                                    oImpuestosTraslados.IVATRAS_Importe = "0";

                                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados);
                                }
                                if (double.Parse(rd[11].ToString()) > 0)
                                {

                                    oImpuestosTraslados = new CFDImpuestosTraslados();
                                    oImpuestosTraslados.IdRegistroTraslado = (oContrador + 1).ToString();
                                    oImpuestosTraslados.Impuesto = "IEPS";
                                    oImpuestosTraslados.Importe = decimal.Parse(rd[11].ToString()).ToString("0.00");
                                    oImpuestosTraslados.Tasa = decimal.Parse(rd[10].ToString()).ToString("0.00");
                                    oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR;
                                    oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR;
                                    oImpuestosTraslados.IEPSTRAS_Importe = "0";
                                    oImpuestosTraslados.IVATRAS_Importe = "0";

                                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados);

                                }
                                oConcepto.RetenerIVA = "0";
                                oConcepto.RetenerISR = "0";

                                oCFD.Conceptos.Add(oConcepto);
                                oContrador = oContrador + 2;
                            }


                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error getting data ConRelClienteObs " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();

                        }
                    }


                    bool bnd = false;
                    string Mensaje1 = "";

                    try
                    {
                        if (!oCFD.Insertar(Cnx, true, false))
                        {

                            foreach (MizarCFD.DAL.DAMensajesSistema.RegistroMensaje oMensaje in oCFD.MensajesSistema.ListaMensajes)
                            {
                                Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto;
                                Inserta_Tbl_FacturasNoGeneradas(oClv_Nota, "O", Mensaje1);
                                result.IdResult = 0;
                                bnd = true;
                                result.Message = "Error al generar la factura fiscal";
                                return result;

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //log.Llenalog(ex.Source.ToString() + " " + ex.Message);
                        throw new Exception(" ERROR DameFacDig_Parte_2Nota " + ex.Message, ex);
                    }

                    GloClv_FacturaCFD = 0;
                    GloClv_FacturaCFD = long.Parse(oCFD.IdCFD);
                    result.IdResult = GloClv_FacturaCFD;

                    if (GloClv_FacturaCFD > 0 && bnd == false)
                    {
                        EnviarFactura.EnvioFactura(GloClv_FacturaCFD);
                    }

                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            SqlCommand COMANDO = new SqlCommand("EXEC ObtienePDFFacturaFiscal " + GloClv_FacturaCFD);

                            COMANDO.CommandTimeout = 0;
                            COMANDO.Connection = connection;
                            rd = COMANDO.ExecuteReader();
                            while (rd.Read())
                            {
                                try
                                {
                                    string name = Guid.NewGuid().ToString() + ".pdf";
                                    string fileName = apppath + "/Reportes/" + name;
                                    byte[] bytes = (byte[])rd[0];
                                    FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate);
                                    fs.Write(bytes, 0, bytes.Length);
                                    fs.Close();
                                    result.urlReporte = name;

                                }
                                catch (Exception ex)
                                {
                                    //throw new Exception("Error No se pudo generar el archivo " + ex.Message, ex);
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error getting data ConRelClienteObs " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                    }


                    UPS_Inserta_Rel_FacturasCFD(oClv_Nota.Value, GloClv_FacturaCFD, "", "O");
                    System.Globalization.CultureInfo r = new System.Globalization.CultureInfo("es-MX");
                    r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
                    System.Threading.Thread.CurrentThread.CurrentCulture = r;

                }
                else
                {
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            SqlCommand COMANDO = new SqlCommand("EXEC ObtienePDFFacturaFiscal " + GloClv_FacturaCFD);
                            COMANDO.CommandType = CommandType.StoredProcedure;
                            COMANDO.CommandTimeout = 0;
                            COMANDO.Connection = connection;

                            while (rd.Read())
                            {
                                try
                                {
                                    string name = Guid.NewGuid().ToString() + ".pdf";
                                    string fileName = apppath + "/Reportes/" + name;
                                    byte[] bytes = (byte[])rd[0];
                                    FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate);
                                    fs.Write(bytes, 0, bytes.Length);
                                    fs.Close();
                                    result.urlReporte = name;

                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Error No se pudo generar el archivo " + ex.Message, ex);
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error getting data ConRelClienteObs " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                    }
                    result.IdResult = 0;
                    result.Message = "La factura fiscal ya existe";
                    return result;
                }

            }
            else
            {
                result.IdResult = -1;
                result.Message = "El contrato  no tiene datos fiscales";
            }

            return result;

        }


        public void Dime_Aque_Compania_FacturarleNOTA(long? oClv_NOTA)
        {
            locID_Compania_Mizart = "";
            locID_Sucursal_Mizart = "";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("Dime_Aque_Compania_FacturarleNOTA");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;

                    SqlParameter prm1 = new SqlParameter("@Clv_NOTA", SqlDbType.BigInt);
                    prm1.Direction = ParameterDirection.Input;
                    prm1.Value = oClv_NOTA;
                    COMANDO.Parameters.Add(prm1);

                    SqlParameter prm2 = new SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50);
                    prm2.Direction = ParameterDirection.Output;
                    prm2.Value = 0;
                    COMANDO.Parameters.Add(prm2);

                    SqlParameter prm3 = new SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50);
                    prm3.Direction = ParameterDirection.Output;
                    prm3.Value = "";
                    COMANDO.Parameters.Add(prm3);
                    COMANDO.ExecuteNonQuery();
                    locID_Compania_Mizart = prm2.Value.ToString();
                    locID_Sucursal_Mizart = prm3.Value.ToString();
                }

                catch (Exception ex)
                {
                    // log.Llenalog(ex.Source.ToString() + " " + ex.Message);
                    throw new Exception("Error Dime_Aque_Compania_FacturarleNOTA" + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }
            }
        }



        public void Usp_Ed_DameDatosFacDigNota(long oCLV_Nota, long oCompania)
        {

            GloFORMADEPAGO = "";
            GloCONDICIONESDEPAGO = "";
            GloMOTIVODESCUENTO = "";
            GloMETODODEPAGO = "";
            GloTIPODECOMPROBANTE = "";
            GloPAGOENPARCIALIDADES = "";
            GloLUGAREXPEDICION = "";
            GloREGIMEN = "";
            GloMONEDA = "";
            GloVERSON = "";
            GloTIPOFACTOR = "";
            GloFORMAPAGO = "";
            GloTIPOCAMBIO = "";
            GloUSOCFD = "";
            GloFECHAVIGENCIA = "";
            GloUUIDRel = "";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("Usp_Ed_DameDatosFacDigNota", connection);
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;


                    SqlParameter PARAMETRO1 = new SqlParameter("@CLV_NOTA", SqlDbType.BigInt);
                    PARAMETRO1.Direction = ParameterDirection.Input;
                    PARAMETRO1.Value = oCLV_Nota;
                    COMANDO.Parameters.Add(PARAMETRO1);

                    SqlParameter PARAMETRO2 = new SqlParameter("@IDCOMPANIA", SqlDbType.Int);
                    PARAMETRO2.Direction = ParameterDirection.Input;
                    PARAMETRO2.Value = oCompania;
                    COMANDO.Parameters.Add(PARAMETRO2);



                    SqlParameter PARAMETRO3 = new SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250);
                    PARAMETRO3.Direction = ParameterDirection.Output;
                    PARAMETRO3.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO3);

                    SqlParameter PARAMETRO4 = new SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250);
                    PARAMETRO4.Direction = ParameterDirection.Output;
                    PARAMETRO4.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO4);

                    SqlParameter PARAMETRO5 = new SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250);
                    PARAMETRO5.Direction = ParameterDirection.Output;
                    PARAMETRO5.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO5);

                    SqlParameter PARAMETRO6 = new SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250);
                    PARAMETRO6.Direction = ParameterDirection.Output;
                    PARAMETRO6.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO6);

                    SqlParameter PARAMETRO7 = new SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 50);
                    PARAMETRO7.Direction = ParameterDirection.Output;
                    PARAMETRO7.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO7);

                    SqlParameter PARAMETRO8 = new SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 50);
                    PARAMETRO8.Direction = ParameterDirection.Output;
                    PARAMETRO8.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO8);

                    SqlParameter PARAMETRO9 = new SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250);
                    PARAMETRO9.Direction = ParameterDirection.Output;
                    PARAMETRO9.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO9);

                    SqlParameter PARAMETRO10 = new SqlParameter("@REGIMEN", SqlDbType.VarChar, 400);
                    PARAMETRO10.Direction = ParameterDirection.Output;
                    PARAMETRO10.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO10);

                    SqlParameter PARAMETRO11 = new SqlParameter("@MONEDA", SqlDbType.VarChar, 15);
                    PARAMETRO11.Direction = ParameterDirection.Output;
                    PARAMETRO11.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO11);

                    SqlParameter PARAMETRO12 = new SqlParameter("@VERSON", SqlDbType.VarChar, 15);
                    PARAMETRO12.Direction = ParameterDirection.Output;
                    PARAMETRO12.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO12);

                    SqlParameter PARAMETRO13 = new SqlParameter("@numCtaPago", SqlDbType.VarChar, 4);
                    PARAMETRO13.Direction = ParameterDirection.Output;
                    PARAMETRO13.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO13);

                    SqlParameter PARAMETRO14 = new SqlParameter("@TipoFactor", SqlDbType.VarChar, 15);
                    PARAMETRO14.Direction = ParameterDirection.Output;
                    PARAMETRO14.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO14);

                    SqlParameter PARAMETRO15 = new SqlParameter("@USOCFD", SqlDbType.VarChar, 15);
                    PARAMETRO15.Direction = ParameterDirection.Output;
                    PARAMETRO15.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO15);

                    SqlParameter PARAMETRO16 = new SqlParameter("@FECHAFACTURA", SqlDbType.VarChar, 15);
                    PARAMETRO16.Direction = ParameterDirection.Output;
                    PARAMETRO16.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO16);

                    SqlParameter PARAMETRO17 = new SqlParameter("@TIPOCAMBIO", SqlDbType.Int, 4);
                    PARAMETRO17.Direction = ParameterDirection.Output;
                    PARAMETRO17.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO17);

                    SqlParameter PARAMETRO18 = new SqlParameter("@UUIDRel", SqlDbType.VarChar, 150);
                    PARAMETRO18.Direction = ParameterDirection.Output;
                    PARAMETRO18.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO18);



                    COMANDO.ExecuteNonQuery();
                    GloFORMADEPAGO = PARAMETRO3.Value.ToString();
                    GloCONDICIONESDEPAGO = PARAMETRO4.Value.ToString();
                    GloMOTIVODESCUENTO = PARAMETRO5.Value.ToString();
                    GloMETODODEPAGO = PARAMETRO6.Value.ToString();
                    GloTIPODECOMPROBANTE = PARAMETRO7.Value.ToString();
                    GloPAGOENPARCIALIDADES = PARAMETRO8.Value.ToString();
                    GloLUGAREXPEDICION = PARAMETRO9.Value.ToString();
                    GloREGIMEN = PARAMETRO10.Value.ToString();
                    GloMONEDA = PARAMETRO11.Value.ToString();
                    GloVERSON = PARAMETRO12.Value.ToString();
                    GlonumCtaPago = PARAMETRO13.Value.ToString();
                    GloTIPOFACTOR = PARAMETRO14.Value.ToString();
                    GloUSOCFD = PARAMETRO15.Value.ToString();
                    GloFECHAVIGENCIA = PARAMETRO16.Value.ToString();
                    GloTIPOCAMBIO = PARAMETRO17.Value.ToString();
                    GloUUIDRel = PARAMETRO18.Value.ToString();

                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Usp_Ed_DameDatosFacDigNota " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }
            }

        }


        public long? BusFacFiscalOledbNota(long? oClv_Factura)
        {
            long BusFacFiscalOledbPago = 0;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("BusFacFiscalNota");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;

                    SqlParameter parametro = new SqlParameter("@CLV_NOTA", SqlDbType.BigInt);
                    parametro.Direction = ParameterDirection.Input;
                    parametro.Value = oClv_Factura;
                    COMANDO.Parameters.Add(parametro);

                    SqlParameter parametro1 = new SqlParameter("@ident", SqlDbType.Int);
                    parametro1.Direction = ParameterDirection.Output;
                    parametro1.Value = 0;
                    COMANDO.Parameters.Add(parametro1);

                    COMANDO.ExecuteNonQuery();
                    BusFacFiscalOledbPago = long.Parse(parametro1.Value.ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("BusFacFiscalMaestroNota " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }
            }
            return BusFacFiscalOledbPago;
        }


        public long Inserta_Tbl_FacturasNoGeneradas(long? oCLV_FActura, string oTipo, string LocObs)
        {

            long result = 0;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("Inserta_Tbl_FacturasNoGeneradas");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;


                    SqlParameter parametro = new SqlParameter("@Clv_Factura", SqlDbType.BigInt);
                    parametro.Direction = ParameterDirection.Input;
                    parametro.Value = oCLV_FActura;
                    COMANDO.Parameters.Add(parametro);

                    SqlParameter parametro1 = new SqlParameter("@TipoFact", SqlDbType.VarChar, 10);
                    parametro1.Direction = ParameterDirection.Input;
                    parametro1.Value = oTipo;
                    COMANDO.Parameters.Add(parametro1);

                    SqlParameter parametro2 = new SqlParameter("@Obs", SqlDbType.VarChar, 8000);
                    parametro2.Direction = ParameterDirection.Input;
                    parametro2.Value = LocObs;
                    COMANDO.Parameters.Add(parametro2);

                    COMANDO.ExecuteNonQuery();
                    result = 1;
                }
                catch (Exception ex)
                {

                }
                return result;

            }
        }

        public void UPS_Inserta_Rel_FacturasCFD(long oCLV_FActura, long oClv_FacturaCFD, string oSerie, string oTipo)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {


                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("UPS_Inserta_Rel_FacturasCFD");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;

                    SqlParameter PARAMETRO1 = new SqlParameter("@Clv_Factura", SqlDbType.BigInt);
                    PARAMETRO1.Direction = ParameterDirection.Input;
                    PARAMETRO1.Value = oCLV_FActura;
                    COMANDO.Parameters.Add(PARAMETRO1);

                    SqlParameter PARAMETRO2 = new SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt);
                    PARAMETRO2.Direction = ParameterDirection.Input;
                    PARAMETRO2.Value = oClv_FacturaCFD;
                    COMANDO.Parameters.Add(PARAMETRO2);

                    SqlParameter PARAMETRO3 = new SqlParameter("@Serie", SqlDbType.VarChar, 5);
                    PARAMETRO3.Direction = ParameterDirection.Input;
                    PARAMETRO3.Value = oSerie;
                    COMANDO.Parameters.Add(PARAMETRO3);

                    SqlParameter PARAMETRO4 = new SqlParameter("@tipo", SqlDbType.VarChar, 1);
                    PARAMETRO4.Direction = ParameterDirection.Input;
                    PARAMETRO4.Value = oTipo;
                    COMANDO.Parameters.Add(PARAMETRO4);
                    COMANDO.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    throw new Exception("Error UPS_Inserta_Rel_FacturasCFD" + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }

            }
        }
    }
}