﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;

namespace SoftvWCFService.functions
{
    public class dbhandler
    {
      


        public  Dictionary<string, Object> ProcedimientoOutPut2(string Procedure, string LocConexion, List<SqlParameter> listParametros)
        {
        Dictionary<string, Object> dicoPar = new Dictionary<string, Object>();
            DataSet dc = new DataSet();
            SqlConnection CON = new SqlConnection(LocConexion);
            SqlCommand cmd = new SqlCommand(Procedure, CON);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;

            if (listParametros.Count > 0)
            {
                cmd.Parameters.AddRange(listParametros.ToArray());
            }
            try
            {
                if (CON.State == ConnectionState.Closed)
                    CON.Open();
                cmd.ExecuteNonQuery();

                listParametros.ForEach(y=> {
                    if(y.Direction == ParameterDirection.Output)
                    {
                        dicoPar.Add(y.ParameterName, y.Value);
                    }
                });

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Source.ToString() + " " + ex.Message);
            }
            finally
            {
                if (CON != null)
                    CON.Close();

            }
            return dicoPar;
        }

        //SOBRE-ESCRITURA DEL MÉTODO QUE SE LE PUEDE ENVIAR UN PROCEDIMIENTO PARA EJECUTARLO CON PARÁMETROS
        public void Inserta(string Procedure, string LocConexion, List<SqlParameter> listParametros)
        {
            DataSet dc = new DataSet();
            SqlConnection CON = new SqlConnection(LocConexion);
            SqlCommand cmd = new SqlCommand(Procedure, CON);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;

            if (listParametros.Count > 0)
            {
                cmd.Parameters.AddRange(listParametros.ToArray());
            }
            try
            {
                if (CON.State == ConnectionState.Closed)
                    CON.Open();
                cmd.ExecuteNonQuery();                

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Source.ToString() + " " + ex.Message);
            }
            finally
            {
                if (CON != null)
                    CON.Close();

            }
        }


        public Dictionary<string, Object> ProcedimientoOutPut(string Procedure, string LocConexion, List<SqlParameter> listParametros)
        {
            Dictionary<string, Object> dicoPar = new Dictionary<string, Object>();
            DataSet dc = new DataSet();
            SqlConnection CON = new SqlConnection(LocConexion);
            SqlCommand cmd = new SqlCommand(Procedure, CON);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;

            if (listParametros.Count > 0)
            {
                cmd.Parameters.AddRange(listParametros.ToArray());
            }
            try
            {
                if (CON.State == ConnectionState.Closed)
                    CON.Open();
                cmd.ExecuteNonQuery();

                listParametros.ForEach(y => {
                    if (y.Direction == ParameterDirection.Output)
                    {
                        dicoPar.Add(y.ParameterName, y.Value);
                    }
                });

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Source.ToString() + " " + ex.Message);
            }
            finally
            {
                if (CON != null)
                    CON.Close();

            }
            return dicoPar;
        }

        //'''''''MÉTODO QUE DEVUELVE UN DATATABLE ENVIANDOLE UN PROCEDIMIENTO CON PARÁMETROS (N NÚMERO DE PARÁMETROS)
        public DataTable ConsultaDT(string Procedure, string LocConexion, List<SqlParameter> listParametros)
        {
            DataTable DT = new DataTable();
            DataSet dc = new DataSet();
            SqlConnection CON = new SqlConnection(LocConexion);
            SqlCommand cmd = new SqlCommand(Procedure, CON);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;

            if (listParametros.Count > 0)
            {
                cmd.Parameters.AddRange(listParametros.ToArray());
            }
            try
            {
                if (CON.State == ConnectionState.Closed)
                    CON.Open();
                SqlDataAdapter DA = new SqlDataAdapter(cmd);
                DA.Fill(DT);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Source.ToString() + " " + ex.Message);
            }
            finally
            {
                if (CON != null)
                    CON.Close();

            }

            return DT;
        }


        //'''''FUNCIÓN QUE DEVUELVE UN VALOR BOOLEANO ENVIANDOLE UN PROCEDIMIENTO CON PARÁMETROS(N NÚMERO DE PARÁMETROS)
        public bool ConsultaBol(string Procedure, string LocConexion, List<SqlParameter> listParametros)
        {

            bool ConsultaBol=false;
            SqlConnection CON = new SqlConnection(LocConexion);
            SqlCommand cmd = new SqlCommand(Procedure, CON);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;

            if (listParametros.Count > 0)
            {
                cmd.Parameters.AddRange(listParametros.ToArray());
            }
            try
            {
                if (CON.State == ConnectionState.Closed)
                    CON.Open();
               // ConsultaBol = cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Source.ToString() + " " + ex.Message);
            }
            finally
            {
                if (CON != null)
                    CON.Close();

            }

            return ConsultaBol;
        }

        //'' MÉTODO QUE DEVUELVE UN PARÁMETRO AL CUÁL SE LE PASA EL NOMBRE,TIPO DE DATO Y EL VALOR DE ESTE (PARAMETROS QUE NO REQUIEREN ESPECÍFICAR LONGITUD)
       public SqlParameter CreateMyParameter(string prmNombre, SqlDbType prmTipo, Object prmValor)
        {
            SqlParameter par = new SqlParameter(prmNombre, prmTipo);
            par.Value = prmValor;
            return par;
        }

        //'SOBRE-ESCRITURA DEL MÉTODO QUE DEVUELVE UN PARÁMETRO AL CUÁL SE LE PASA EL NOMBRE,TIPO DE DATO,TAMAÑO Y EL VALOR DE ESTE (PARAMETROS QUE REQUIEREN ESPECÍFICAR LONGITUD)
        public SqlParameter CreateMyParameter(string prmNombre, SqlDbType prmTipo, Object prmValor,int prmTamanio)
        {
            SqlParameter par = new SqlParameter(prmNombre, prmTipo, prmTamanio);
            par.Value = prmValor;
            return par;
        }

        public SqlParameter CreateMyParameter(string prmNombre, ParameterDirection prmDireccion, SqlDbType prmTipo, Object prmValor)
        {
            SqlParameter par = new SqlParameter(prmNombre, prmTipo);
            par.Value = prmValor;
            par.Direction = prmDireccion;
            return par;
        }

        //'SOBRE-ESCRITURA DEL MÉTODO QUE DEVUELVE UN PARÁMETRO AL CUÁL SE LE PASA EL NOMBRE,TIPO DE DATO,TAMAÑO,DIRECCIÓN Y EL VALOR DE ESTE (PARAMETROS QUE REQUIEREN ESPECÍFICAR LONGITUD)
        public SqlParameter CreateMyParameter(string prmNombre, SqlDbType prmTipo, int prmTamanio, ParameterDirection prmDireccion, Object prmValor)
        {
            SqlParameter par = new SqlParameter(prmNombre, prmTipo, prmTamanio);
            par.Direction = prmDireccion;
            par.Value = prmValor;
            return par;
        }

        public SqlParameter CreateMyParameter(string prmNombre, SqlDbType prmTipo,  ParameterDirection prmDireccion)
        {
            SqlParameter par = new SqlParameter(prmNombre, prmTipo);
            par.Direction = prmDireccion;            
            return par;
        }

        public SqlParameter CreateMyParameter(string prmNombre, ParameterDirection prmDireccion, SqlDbType prmTipo,int prmTamanio)
        {
            SqlParameter par = new SqlParameter(prmNombre, prmTipo, prmTamanio);
            par.Direction = prmDireccion;
            return par;
        }

      //  'SOBRE-ESCRITURA DEL MÉTODO QUE DEVUELVE UN DATASET DE UN PROCEDIMIENTO CON PARÁMETROS Y ASIGNACIÓN DE NOMBRE A LAS TABLAS QUE DEVUELVE EL DATASET  (TABLE.NAME)
        public DataSet ConsultaDS(string Procedure,List<string> ltablesnames,string locconexion, List<SqlParameter> listParametros)
        {
            DataSet dc = new DataSet();
            SqlConnection CON = new SqlConnection(locconexion);
            SqlCommand cmd = new SqlCommand(Procedure, CON);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;

            if (listParametros.Count > 0)
            {
                cmd.Parameters.AddRange(listParametros.ToArray());
            }
            try
            {
                if (CON.State == ConnectionState.Closed)
                    CON.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.Fill(dc);

                for (int i = 0; i < ltablesnames.Count - 1; i++)
                {
                    dc.Tables[i].TableName = ltablesnames[i];
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Source.ToString() + " " + ex.Message);
            }
            finally
            {
                if (CON != null)
                    CON.Close();
            }  

            return dc;
        }


        public DataSet ConsultaDS(string Procedure,  string locconexion, List<SqlParameter> listParametros)
        {
            DataSet dc = new DataSet();
            SqlConnection CON = new SqlConnection(locconexion);
            SqlCommand cmd = new SqlCommand(Procedure, CON);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;

            if (listParametros.Count > 0)
            {
                cmd.Parameters.AddRange(listParametros.ToArray());
            }
            try
            {
                if (CON.State == ConnectionState.Closed)
                    CON.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.Fill(dc);

                

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Source.ToString() + " " + ex.Message);
            }
            finally
            {
                if (CON != null)
                    CON.Close();
            }

            return dc;
        }
    }
}