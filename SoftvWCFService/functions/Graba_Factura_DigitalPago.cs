﻿using MizarCFD.BRL;
using MizarCFD.DAL;
using softv.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Xml;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
namespace SoftvWCFService.functions
{

    public class Graba_Factura_DigitalPago
    {
        public string locID_Sucursal_Mizart = "";
        public string locID_Compania_Mizart = "";
        public string GloFORMADEPAGO = "";
        public string GloCONDICIONESDEPAGO = "";
        public string GloMOTIVODESCUENTO = "";
        public string GloMETODODEPAGO = "";
        public string GloTIPODECOMPROBANTE = "";
        public string GloPAGOENPARCIALIDADES = "";
        public string GloLUGAREXPEDICION = "";
        public string GloREGIMEN = "";
        public string GloMONEDA = "";
        public string GloVERSON = "";
        public string GloTIPOFACTOR = "";
        public string GloFORMAPAGO = "";
        public string GloTIPOCAMBIO = "";
        public string GloUSOCFD = "";
        public string GloFECHAVIGENCIA = "";
        public string GloSERIE = "";
        public string GloFOLIO = "";
        public string GloNUMPARCIALIDAD = "";
        public string GloSALDOANTERIOR = "";
        public string GloSALDONUEVO = "";
        public string GloUUIDRel = "";
        public string GloID_CFD = "";
        public string GloMONTOPAGO = "";
        public string GloMONTOORIGINAL = "";
        public string GlonumCtaPago = "";
        public string GloTxtUlt4DigCheque = "";
        public long GloClv_FacturaCFD = 0;
        public DateTime GloFECHAPAGO;
        Graba_Factura_DigitalMaestro grabaFacMaes = new Graba_Factura_DigitalMaestro();
        string ConnectionString = ConfigurationManager.ConnectionStrings["NEWSOFTV"].ConnectionString;
        string ConnectionString2 = ConfigurationManager.ConnectionStrings["HL"].ConnectionString;
        bool TimbrePrueba = bool.Parse(ConfigurationManager.AppSettings["TimbrePrueba"].ToString());
        EnviarFactura EnviarFactura = new EnviarFactura();

        public ResultEntity GetGraba_Factura_DigitalPago(long? oClv_Factura, long? oIden)
        {
            ResultEntity result = new ResultEntity();
            oIden = BusFacFiscalOledbPago(oClv_Factura);

            if (oIden > 0)
            {
                Dime_Aque_Compania_FacturarlePago(oClv_Factura);
                long ClvFacturaCfd = 0;
                ClvFacturaCfd = grabaFacMaes.SP_DIMESIEXISTEFACTURADIGITAL(oClv_Factura, "P").Value;
                if (ClvFacturaCfd == 0)
                {
                    string oTotalConPuntos = "";
                    string oSubTotal = "";
                    string oTotalSinPuntos = "";
                    string oDescuento = "";
                    string oiva = "";
                    string oieps = "";
                    string oTasaIva = "";
                    string oTasaIeps = "";
                    string oFecha = "";
                    string oTotalImpuestos = "";

                    Usp_Ed_DameDatosFacDigPago(oClv_Factura, long.Parse(locID_Compania_Mizart));

                    MizarCFD.BRL.CFD oCFD = new MizarCFD.BRL.CFD();
                    long oId_AsociadoLlave = 0;
                    oId_AsociadoLlave = oIden.Value;

                    DAConexion Cnx = new DAConexion("HL", "sa", "sa");
                    try
                    {
                        oCFD = new MizarCFD.BRL.CFD();
                        oCFD.Inicializar();
                        oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Pago;

                        MizarCFD.BRL.CFD oCFDFacturaOriginal = new MizarCFD.BRL.CFD();
                        oCFDFacturaOriginal.IdCFD = GloID_CFD;
                        oCFDFacturaOriginal.Consultar(Cnx);

                        if (locID_Sucursal_Mizart.Length > 0 && locID_Sucursal_Mizart != "0")
                        {
                            oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart);
                        }
                        else
                        {
                            oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart);
                        }
                        //Pagos
                        oCFD.PrepararDatosReceptorPorId(Cnx, oId_AsociadoLlave.ToString(), "1", "0");


                        MizarCFD.BRL.CFDPagos oPagos = new MizarCFD.BRL.CFDPagos();
                        oPagos.Descripcion = "Pago";
                        oPagos.TipoPago = GloFORMAPAGO;
                        oPagos.idMoneda = GloMONEDA;

                        //Dólares - Dólares no se captura y se pone el de la original
                        if (oCFDFacturaOriginal.Moneda.Contains("mericano") && GloMONEDA == "USD")
                        {
                            oPagos.TipoCambio = oCFDFacturaOriginal.TipoCambio;//GloTIPOCAMBIO;//(1 / decimal.Parse(GloTIPOCAMBIO)).ToString();
                        }
                        else if (oCFDFacturaOriginal.Moneda.Contains("exicano") && GloMONEDA == "MXN")
                        {
                            oPagos.TipoCambio = "1.00";
                        }

                        oPagos.MontoTotalOriginal = GloMONTOPAGO;
                        oPagos.FechaTransaccion = GloFECHAPAGO;
                        oPagos.Version = "1.0";
                        oPagos.MontoPago = GloMONTOPAGO;



                        //oPagos.MontoPago = GloMONTOPAGO;
                        //oPagos.SaldoAnterior = GloSALDOANTERIOR;
                        //oPagos.SaldoNuevo = GloSALDONUEVO;
                        //oPagos.Version = GloVERSON;
                        //oPagos.NumParcialidad = GloNUMPARCIALIDAD;

                        //Pago Complemento
                        MizarCFD.BRL.CFDPagosComplemento oPagoComp = new MizarCFD.BRL.CFDPagosComplemento();
                        oPagoComp.IDCFD = oCFDFacturaOriginal.IdCFD;
                        oPagoComp.IdDocumento = GloUUIDRel;
                        oPagoComp.Serie = oCFDFacturaOriginal.Serie;//GloSERIE;
                        oPagoComp.Folio = oCFDFacturaOriginal.Folio;//GloFOLIO;
                        oPagoComp.MonedaDR = oCFDFacturaOriginal.Moneda.Contains("Peso") ? "MXN" : "USD";//GloMONEDA;

                        //TipoCambioDR= NO SE DEBE LLENAR CUANDO MonedaP y MonedaDR son iguales
                        if ((oCFDFacturaOriginal.Moneda == "Dolar americano" && GloMONEDA == "MXN"))
                        {
                            oPagoComp.TipoCambioDR = GloTIPOCAMBIO;
                        }


                        oPagoComp.MetodoPagoDR = oCFDFacturaOriginal.MetodoDePago;
                        oPagoComp.SaldoAnterior = GloSALDOANTERIOR;
                        oPagoComp.SaldoNuevo = GloSALDONUEVO;
                        oPagoComp.MontoPagoDR = GloMONTOORIGINAL;
                        oPagoComp.NumParcialidad = GloNUMPARCIALIDAD;
                        oPagoComp.MontoOriginal = GloMONTOPAGO;

                        oPagos.DoctoRel.Add(oPagoComp);


                        oCFD.IdCFD = DAUtileriasSQL.ObtenerSiguienteId(Cnx, "id_cfd", "cfd").ToString();
                        oPagos.IdCFDGenerado = oCFD.IdCFD;
                        oCFD.Pagos.Add(oPagos);
                        //oCFD.NumeroCertificado = oCFDFacturaOriginal.NumeroCertificado;

                        oCFD.EsTimbrePrueba = TimbrePrueba;
                        oCFD.Timbrador = CFD.PAC.Mizar;
                        oCFD.Version = GloVERSON;
                        if (GloMONEDA == "USD")
                        {
                            oCFD.IdMoneda = "2";
                        }
                        else
                        {
                            oCFD.IdMoneda = "1";//1 = Pesos , Pesos = 2 USD  'oFecha
                        }
                        oCFD.FormaDePago = GloFORMAPAGO;
                        // 'oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                        oCFD.SubTotal = "0.00";
                        oCFD.Descuento = "0.00";
                        oCFD.MotivoDescuento = GloMOTIVODESCUENTO;
                        oCFD.Total = "0.00";
                        //oCFD.MetodoDePago = GloMETODODEPAGO;
                        oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE;
                        oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE;
                        oCFD.PagoEnParcialiades = "0";
                        oCFD.LugarExpedicion = oCFDFacturaOriginal.LugarExpedicion;
                        oCFD.CuentaPago = GlonumCtaPago;
                        oCFD.Moneda = "XXX";
                        oCFD.UsoCFDI = GloUSOCFD;
                        oCFD.EsNotaCredito = "2";

                        //oCFD.TipoRelacion = "02";
                        //oCFD.UUIDRelacionados = GloUUIDRel;
                        //oCFD.UUIDRel.Add(GloUUIDRel);
                        // 'oCFD.FechaVencimiento = Date.Parse(GloFECHAVIGENCIA)
                        // 'oCFD.CondicionesDePago = ""
                        //oCFD.TipoCambio = GloTIPOCAMBIO;
                        //'oCFD.IdEstatus = 1

                        var ComplementoPago = oPagos.ObtenerXMLCFDi();
                        oCFD.Complemento.XMLPlantilla = ComplementoPago.OuterXml;
                        oCFD.Complemento.IdPlantillaXSD = "0";


                        CFDConceptos oConcepto = new CFDConceptos();
                        oConcepto.idClaveProdServ = "84111506";
                        oConcepto.Cantidad = "1";
                        oConcepto.idUnidadMedida = "ACT";
                        oConcepto.Descripcion = "Pago";
                        oConcepto.ValorUnitario = "0";
                        oConcepto.Importe = "0";

                        oConcepto.IVARET_Importe = "0";
                        oConcepto.IVATRAS_Importe = "0";
                        oConcepto.IEPSTRAS_Importe = "0";
                        oConcepto.ISRRET_Importe = "0";
                        oCFD.Conceptos.Add(oConcepto);

                        bool bnd = false;
                        string Mensaje1 = "";

                        string a = JsonConvert.SerializeObject(oCFD);


                        try
                        {
                            if (!oCFD.Insertar(Cnx, true, true))
                            {

                                foreach (MizarCFD.DAL.DAMensajesSistema.RegistroMensaje oMensaje in oCFD.MensajesSistema.ListaMensajes)
                                {
                                    Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto;
                                    Inserta_Tbl_FacturasNoGeneradas(oClv_Factura, "P", Mensaje1);
                                    bnd = true;
                                }
                            }
                            else
                            {
                                GloClv_FacturaCFD = 0;
                                GloClv_FacturaCFD = long.Parse(oCFD.IdCFD);
                                result.IdResult = GloClv_FacturaCFD;

                                UPS_Inserta_Rel_FacturasCFD(oClv_Factura.Value, GloClv_FacturaCFD, "", "P");
                            }
                        }
                        catch (Exception ex)
                        {
                            //  log.Llenalog(ex.Source.ToString() + " " + ex.Message);
                            throw new Exception("Inserta_Tbl_FacturasNoGeneradas " + ex.Message, ex);
                        }



                        if (GloClv_FacturaCFD > 0 && bnd == false)
                        {
                            EnviarFactura.EnvioFactura(GloClv_FacturaCFD);
                        }
                        using (SqlConnection connection = new SqlConnection(ConnectionString))
                        {
                            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                            IDataReader rd = null;
                            try
                            {
                                if (connection.State == ConnectionState.Closed)
                                    connection.Open();
                                SqlCommand COMANDO = new SqlCommand("EXEC ObtienePDFFacturaFiscal " + GloClv_FacturaCFD);
                                COMANDO.CommandTimeout = 0;
                                COMANDO.Connection = connection;
                                rd = COMANDO.ExecuteReader();
                                while (rd.Read())
                                {
                                    try
                                    {
                                        string name = Guid.NewGuid().ToString() + ".pdf";
                                        string fileName = apppath + "/Reportes/" + name;
                                        byte[] bytes = (byte[])rd[0];
                                        FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate);
                                        fs.Write(bytes, 0, bytes.Length);
                                        fs.Close();

                                        result.urlReporte = name;
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new Exception("Error No se pudo generar el archivo " + ex.Message, ex);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Error getting data ConRelClienteObs " + ex.Message, ex);
                            }
                            finally
                            {
                                if (connection != null)
                                    connection.Close();
                                if (rd != null)
                                    rd.Close();
                            }
                        }

                        GloTxtUlt4DigCheque = "";
                        System.Globalization.CultureInfo r = new System.Globalization.CultureInfo("es-MX");
                        r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
                        System.Threading.Thread.CurrentThread.CurrentCulture = r;

                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error GetGraba_Factura_DigitalPago" + ex.Message, ex);
                    }

                }
                else
                {
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            SqlCommand COMANDO = new SqlCommand("EXEC ObtienePDFFacturaFiscal " + GloClv_FacturaCFD);
                            COMANDO.CommandType = CommandType.StoredProcedure;
                            COMANDO.CommandTimeout = 0;
                            COMANDO.Connection = connection;

                            while (rd.Read())
                            {
                                try
                                {
                                    string name = Guid.NewGuid().ToString() + ".pdf";
                                    string fileName = apppath + "/Reportes/" + name;
                                    byte[] bytes = (byte[])rd[0];
                                    FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate);
                                    fs.Write(bytes, 0, bytes.Length);
                                    fs.Close();
                                    result.urlReporte = name;

                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Error No se pudo generar el archivo " + ex.Message, ex);
                                }





                            }

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error getting data ConRelClienteObs " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                    }
                    result.IdResult = 0;
                    result.Message = "La factura fiscal ya existe";
                    return result;
                }




            }
            else
            {
                result.IdResult = -1;
                result.Message = "El contrato maestro no tiene datos fiscales";
            }

            return result;

        }




        public long Inserta_Tbl_FacturasNoGeneradas(long? oCLV_FActura, string oTipo, string LocObs)
        {

            long result = 0;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand COMANDO = new SqlCommand("Inserta_Tbl_FacturasNoGeneradas");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;


                    SqlParameter parametro = new SqlParameter("@Clv_Factura", SqlDbType.BigInt);
                    parametro.Direction = ParameterDirection.Input;
                    parametro.Value = oCLV_FActura;
                    COMANDO.Parameters.Add(parametro);

                    SqlParameter parametro1 = new SqlParameter("@TipoFact", SqlDbType.VarChar, 10);
                    parametro1.Direction = ParameterDirection.Input;
                    parametro1.Value = oTipo;
                    COMANDO.Parameters.Add(parametro1);

                    SqlParameter parametro2 = new SqlParameter("@Obs", SqlDbType.VarChar, 8000);
                    parametro2.Direction = ParameterDirection.Input;
                    parametro2.Value = LocObs;
                    COMANDO.Parameters.Add(parametro2);

                    COMANDO.ExecuteNonQuery();
                    result = 1;

                    connection.Close();
                }
                catch (Exception ex)
                {

                }
                return result;

            }
        }


        public void Dime_Aque_Compania_FacturarlePago(long? oClv_Factura)
        {
            locID_Compania_Mizart = "";
            locID_Sucursal_Mizart = "";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("Dime_Aque_Compania_FacturarlePago");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;

                    SqlParameter prm1 = new SqlParameter("@Clv_Pago", SqlDbType.BigInt);
                    prm1.Direction = ParameterDirection.Input;
                    prm1.Value = oClv_Factura;
                    COMANDO.Parameters.Add(prm1);

                    SqlParameter prm2 = new SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50);
                    prm2.Direction = ParameterDirection.Output;
                    prm2.Value = 0;
                    COMANDO.Parameters.Add(prm2);

                    SqlParameter prm3 = new SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50);
                    prm3.Direction = ParameterDirection.Output;
                    prm3.Value = "";
                    COMANDO.Parameters.Add(prm3);
                    COMANDO.ExecuteNonQuery();
                    locID_Compania_Mizart = prm2.Value.ToString();
                    locID_Sucursal_Mizart = prm3.Value.ToString();
                }

                catch (Exception ex)
                {
                    // log.Llenalog(ex.Source.ToString() + " " + ex.Message);
                    throw new Exception("Error Dime_Aque_Compania_FacturarleMaestro" + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }
            }
        }

        public void UPS_Inserta_Rel_FacturasCFD(long oCLV_FActura, long oClv_FacturaCFD, string oSerie, string oTipo)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {


                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("UPS_Inserta_Rel_FacturasCFD");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;

                    SqlParameter PARAMETRO1 = new SqlParameter("@Clv_Factura", SqlDbType.BigInt);
                    PARAMETRO1.Direction = ParameterDirection.Input;
                    PARAMETRO1.Value = oCLV_FActura;
                    COMANDO.Parameters.Add(PARAMETRO1);

                    SqlParameter PARAMETRO2 = new SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt);
                    PARAMETRO2.Direction = ParameterDirection.Input;
                    PARAMETRO2.Value = oClv_FacturaCFD;
                    COMANDO.Parameters.Add(PARAMETRO2);

                    SqlParameter PARAMETRO3 = new SqlParameter("@Serie", SqlDbType.VarChar, 5);
                    PARAMETRO3.Direction = ParameterDirection.Input;
                    PARAMETRO3.Value = oSerie;
                    COMANDO.Parameters.Add(PARAMETRO3);

                    SqlParameter PARAMETRO4 = new SqlParameter("@tipo", SqlDbType.VarChar, 1);
                    PARAMETRO4.Direction = ParameterDirection.Input;
                    PARAMETRO4.Value = oTipo;
                    COMANDO.Parameters.Add(PARAMETRO4);
                    COMANDO.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    throw new Exception("Error UPS_Inserta_Rel_FacturasCFD" + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }

            }
        }

        public long? BusFacFiscalOledbPago(long? oClv_Factura)
        {
            long BusFacFiscalOledbPago = 0;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("BusFacFiscalPago");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;

                    SqlParameter parametro = new SqlParameter("@Clv_Pago", SqlDbType.BigInt);
                    parametro.Direction = ParameterDirection.Input;
                    parametro.Value = oClv_Factura;
                    COMANDO.Parameters.Add(parametro);

                    SqlParameter parametro1 = new SqlParameter("@ident", SqlDbType.Int);
                    parametro1.Direction = ParameterDirection.Output;
                    parametro1.Value = 0;
                    COMANDO.Parameters.Add(parametro1);

                    COMANDO.ExecuteNonQuery();
                    BusFacFiscalOledbPago = long.Parse(parametro1.Value.ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("BusFacFiscalPago " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }
            }
            return BusFacFiscalOledbPago;
        }


        public void Usp_Ed_DameDatosFacDigPago(long? oCLV_FActura, long? oCompania)
        {
            GloFORMADEPAGO = "";
            GloCONDICIONESDEPAGO = "";
            GloMOTIVODESCUENTO = "";
            GloMETODODEPAGO = "";
            GloTIPODECOMPROBANTE = "";
            GloPAGOENPARCIALIDADES = "";
            GloLUGAREXPEDICION = "";
            GloREGIMEN = "";
            GloMONEDA = "";
            GloVERSON = "";
            GloTIPOFACTOR = "";
            GloFORMAPAGO = "";
            GloTIPOCAMBIO = "";
            GloUSOCFD = "";
            GloFECHAVIGENCIA = "";
            GloSERIE = "";
            GloFOLIO = "";
            GloNUMPARCIALIDAD = "";
            GloSALDOANTERIOR = "";
            GloSALDONUEVO = "";
            GloUUIDRel = "";
            GloID_CFD = "";
            GloMONTOPAGO = "";
            GloMONTOORIGINAL = "";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand COMANDO = new SqlCommand("Usp_Ed_DameDatosFacDigPago");
                    COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;

                    SqlParameter parametro1 = new SqlParameter("@Clv_Pago", SqlDbType.BigInt);
                    parametro1.Direction = ParameterDirection.Input;
                    parametro1.Value = oCLV_FActura;
                    COMANDO.Parameters.Add(parametro1);


                    SqlParameter parametro2 = new SqlParameter("@IDCOMPANIA", SqlDbType.BigInt);
                    parametro2.Direction = ParameterDirection.Input;
                    parametro2.Value = oCompania;
                    COMANDO.Parameters.Add(parametro2);

                    SqlParameter PARAMETRO3 = new SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250);
                    PARAMETRO3.Direction = ParameterDirection.Output;
                    PARAMETRO3.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO3);

                    SqlParameter PARAMETRO4 = new SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250);
                    PARAMETRO4.Direction = ParameterDirection.Output;
                    PARAMETRO4.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO4);

                    SqlParameter PARAMETRO5 = new SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250);
                    PARAMETRO5.Direction = ParameterDirection.Output;
                    PARAMETRO5.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO5);

                    SqlParameter PARAMETRO6 = new SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250);
                    PARAMETRO6.Direction = ParameterDirection.Output;
                    PARAMETRO6.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO6);

                    SqlParameter PARAMETRO7 = new SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250);
                    PARAMETRO7.Direction = ParameterDirection.Output;
                    PARAMETRO7.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO7);

                    SqlParameter PARAMETRO8 = new SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250);
                    PARAMETRO8.Direction = ParameterDirection.Output;
                    PARAMETRO8.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO8);

                    SqlParameter PARAMETRO9 = new SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250);
                    PARAMETRO9.Direction = ParameterDirection.Output;
                    PARAMETRO9.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO9);

                    SqlParameter PARAMETRO10 = new SqlParameter("@REGIMEN", SqlDbType.VarChar, 400);
                    PARAMETRO10.Direction = ParameterDirection.Output;
                    PARAMETRO10.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO10);

                    SqlParameter PARAMETRO11 = new SqlParameter("@MONEDA", SqlDbType.VarChar, 15);
                    PARAMETRO11.Direction = ParameterDirection.Output;
                    PARAMETRO11.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO11);

                    SqlParameter PARAMETRO12 = new SqlParameter("@VERSON", SqlDbType.VarChar, 15);
                    PARAMETRO12.Direction = ParameterDirection.Output;
                    PARAMETRO12.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO12);

                    SqlParameter PARAMETRO13 = new SqlParameter("@numCtaPago", SqlDbType.VarChar, 4);
                    PARAMETRO13.Direction = ParameterDirection.Output;
                    PARAMETRO13.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO13);

                    SqlParameter PARAMETRO14 = new SqlParameter("@TIPOFACTOR", SqlDbType.VarChar, 4);
                    PARAMETRO14.Direction = ParameterDirection.Output;
                    PARAMETRO14.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO14);

                    SqlParameter PARAMETRO15 = new SqlParameter("@USOCFD", SqlDbType.VarChar, 4);
                    PARAMETRO15.Direction = ParameterDirection.Output;
                    PARAMETRO15.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO15);

                    SqlParameter PARAMETRO16 = new SqlParameter("@FECHAFACTURA", SqlDbType.VarChar, 20);
                    PARAMETRO16.Direction = ParameterDirection.Output;
                    PARAMETRO16.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO16);

                    SqlParameter PARAMETRO17 = new SqlParameter("@TIPOCAMBIO", SqlDbType.Money);
                    PARAMETRO17.Direction = ParameterDirection.Output;
                    PARAMETRO17.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO17);

                    SqlParameter PARAMETRO18 = new SqlParameter("@SERIE", SqlDbType.VarChar, 4);
                    PARAMETRO18.Direction = ParameterDirection.Output;
                    PARAMETRO18.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO18);

                    SqlParameter PARAMETRO19 = new SqlParameter("@FOLIO", SqlDbType.BigInt);
                    PARAMETRO19.Direction = ParameterDirection.Output;
                    PARAMETRO19.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO19);

                    SqlParameter PARAMETRO20 = new SqlParameter("@NUMPARCIALIDAD", SqlDbType.Int);
                    PARAMETRO20.Direction = ParameterDirection.Output;
                    PARAMETRO20.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO20);

                    SqlParameter PARAMETRO21 = new SqlParameter("@SALDOANTERIOR", SqlDbType.Money);
                    PARAMETRO21.Direction = ParameterDirection.Output;
                    PARAMETRO21.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO21);

                    SqlParameter PARAMETRO22 = new SqlParameter("@SALDONUEVO", SqlDbType.Money);
                    PARAMETRO22.Direction = ParameterDirection.Output;
                    PARAMETRO22.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO22);

                    SqlParameter PARAMETRO23 = new SqlParameter("@UUIDRel", SqlDbType.VarChar, 150);
                    PARAMETRO23.Direction = ParameterDirection.Output;
                    PARAMETRO23.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO23);

                    SqlParameter PARAMETRO24 = new SqlParameter("@ID_CFD", SqlDbType.BigInt);
                    PARAMETRO24.Direction = ParameterDirection.Output;
                    PARAMETRO24.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO24);

                    SqlParameter PARAMETRO25 = new SqlParameter("@MONTOPAGO", SqlDbType.Money);
                    PARAMETRO25.Direction = ParameterDirection.Output;
                    PARAMETRO25.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO25);

                    SqlParameter PARAMETRO26 = new SqlParameter("@MONTOORIGINAL", SqlDbType.Money);
                    PARAMETRO26.Direction = ParameterDirection.Output;
                    PARAMETRO26.Value = "";
                    COMANDO.Parameters.Add(PARAMETRO26);

                    SqlParameter PARAMETRO27 = new SqlParameter("@FechaPago", SqlDbType.DateTime);
                    PARAMETRO27.Direction = ParameterDirection.Output;
                    PARAMETRO27.Value = "20180605";
                    COMANDO.Parameters.Add(PARAMETRO27);

                    COMANDO.ExecuteNonQuery();
                    GloFORMAPAGO = PARAMETRO3.Value.ToString();
                    GloCONDICIONESDEPAGO = PARAMETRO4.Value.ToString();
                    GloMOTIVODESCUENTO = PARAMETRO5.Value.ToString();
                    GloMETODODEPAGO = PARAMETRO6.Value.ToString();
                    GloTIPODECOMPROBANTE = PARAMETRO7.Value.ToString();
                    GloPAGOENPARCIALIDADES = PARAMETRO8.Value.ToString();
                    GloLUGAREXPEDICION = PARAMETRO9.Value.ToString();
                    GloREGIMEN = PARAMETRO10.Value.ToString();
                    GloMONEDA = PARAMETRO11.Value.ToString();
                    GloVERSON = PARAMETRO12.Value.ToString();
                    GlonumCtaPago = PARAMETRO13.Value.ToString();
                    GloTIPOFACTOR = PARAMETRO14.Value.ToString();
                    GloUSOCFD = PARAMETRO15.Value.ToString();
                    GloFECHAVIGENCIA = PARAMETRO16.Value.ToString();
                    GloTIPOCAMBIO = decimal.Parse(PARAMETRO17.Value.ToString()).ToString("0.0000");
                    GloSERIE = PARAMETRO18.Value.ToString();
                    GloFOLIO = PARAMETRO19.Value.ToString();
                    GloNUMPARCIALIDAD = PARAMETRO20.Value.ToString();
                    GloSALDOANTERIOR = decimal.Parse(PARAMETRO21.Value.ToString()).ToString("0.00");
                    GloSALDONUEVO = decimal.Parse(PARAMETRO22.Value.ToString()).ToString("0.00");
                    GloUUIDRel = PARAMETRO23.Value.ToString();
                    GloID_CFD = PARAMETRO24.Value.ToString();
                    GloMONTOPAGO = decimal.Parse(PARAMETRO25.Value.ToString()).ToString("0.00");
                    GloMONTOORIGINAL = decimal.Parse(PARAMETRO26.Value.ToString()).ToString("0.00");
                    GloFECHAPAGO = DateTime.Parse(PARAMETRO27.Value.ToString());


                }
                catch (Exception ex)
                {
                    throw new Exception("BusFacFiscalPago " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();

                }
            }



        }



        public ResultEntity GetImprimeFacturaFiscalpago(long? oClv_Factura)
        {
            long? ClvFacturaCfd = 0;
            ResultEntity result = new ResultEntity();
            ClvFacturaCfd = grabaFacMaes.SP_DIMESIEXISTEFACTURADIGITAL(oClv_Factura, "P").Value;

            if (ClvFacturaCfd == 0)
            {
                result.IdResult = 0;
                result.Message = "La factura no se puede enviar ya que no cuenta con folio fiscal";
                return result;
            }

            DAConexion Cnx = new DAConexion("HL", "sa", "sa");
            MizarCFD.DAL.DAUtileriasSQL odas = new MizarCFD.DAL.DAUtileriasSQL();
            MizarCFD.BRL.CFD oCFD = new MizarCFD.BRL.CFD();
            oCFD.IdCFD = ClvFacturaCfd.ToString();
            oCFD.IdCompania = "1";
            oCFD.Consultar(Cnx);
            oCFD.GenerarArchivosXMLPDF(Cnx);

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    SqlCommand COMANDO = new SqlCommand("EXEC ObtienePDFFacturaFiscal " + ClvFacturaCfd);
                    // COMANDO.CommandType = CommandType.StoredProcedure;
                    COMANDO.CommandTimeout = 0;
                    COMANDO.Connection = connection;
                    rd = COMANDO.ExecuteReader();
                    while (rd.Read())
                    {
                        try
                        {
                            string name = Guid.NewGuid().ToString() + ".pdf";
                            string fileName = apppath + "/Reportes/" + name;
                            byte[] bytes = (byte[])rd[0];
                            FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate);
                            fs.Write(bytes, 0, bytes.Length);
                            fs.Close();
                            result.IdResult = 1;
                            result.urlReporte = name;
                            result.Message = "La factura se imprimio correctamente";
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error No se pudo generar el archivo " + ex.Message, ex);
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ObtienePDFFacturaFiscal " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return result;

        }


        public ResultEntity GetEnviaFacturaFiscalpago(long? oClv_Factura)
        {
            ResultEntity result = new ResultEntity();
            try
            {
                long? ClvFacturaCfd = 0;
                ClvFacturaCfd = ClvFacturaCfd = grabaFacMaes.SP_DIMESIEXISTEFACTURADIGITAL(oClv_Factura, "P").Value;

                if (ClvFacturaCfd == 0)
                {
                    result.IdResult = 0;
                    result.Message = "La factura no se puede enviar ya que no cuenta con folio fiscal";
                    return result;
                }


                EnviarFactura.EnvioFactura(ClvFacturaCfd);
                result.IdResult = 1;
                result.Message = "La factura se envió correctamente";
            }
            catch (Exception e)
            {
                result.IdResult = 0;
                result.Message = e.InnerException.Message;
                return result;
            }
            return result;

        }


    }
}