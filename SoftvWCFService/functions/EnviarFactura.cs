﻿using MizarCFD.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SoftvWCFService.functions
{
    public class EnviarFactura
    {
        dbhandler db = new dbhandler();
        Log log = new Log();
        string ConnectionString = ConfigurationManager.ConnectionStrings["NEWSOFTV"].ConnectionString;
        public int? EnvioFactura( long  ? oClv_Factura)
        {
            int result = 0;
            try
            {

                //List<SqlParameter> listParametros = new List<SqlParameter>();
                //listParametros.Add(db.CreateMyParameter("@Clv_Factura", SqlDbType.BigInt, oClv_Factura));
                //listParametros.Add(db.CreateMyParameter("@Envia", SqlDbType.Int, ParameterDirection.Output));
                //Dictionary<string, Object> dicoPar = db.ProcedimientoOutPut("ValidaEnvioFacturasCFD", ConnectionString, listParametros);

                //if (Int32.Parse(dicoPar["@Envia"].ToString())==1){

                    string CadenaOriginal = "";
                    string oSerie = "";
                    string Cantidad_Con_Letra = "";

                    DAConexion Cnx = new DAConexion("HL", "sa", "sa");
                    MizarCFD.DAL.DAUtileriasSQL odas = new MizarCFD.DAL.DAUtileriasSQL();
                    MizarCFD.BRL.CFD oCFD = new MizarCFD.BRL.CFD();
                    string oPath = "";
                    oCFD.IdCFD = oClv_Factura.ToString();
                    oCFD.IdCompania = "1";
                    oCFD.Consultar(Cnx);
                    oCFD.GenerarArchivosXMLPDF(Cnx);
                    oCFD.EnviarEmail(Cnx);
                //}       
                result = 1;

            }
            catch (Exception ex)
            {
                log.Llenalog(ex.Source.ToString()+" "+ex.Message);
            }

            return result;
        }

    }
}