﻿

using SoftvWCFService.Contracts;
using System;
using System.Collections.Generic;

using System.IO;
using System.Linq;
using System.Net;
using Softv.Entities;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
using SoftvWCFService.functions;
using softv.Entities;

namespace SoftvWCFService
{
    [ScriptService]
    public partial class SoftvWCFService:IFacturacionSoftv   
    {
       
        Graba_Factura_DigitalMaestro Graba_Factura_DigitalMaestro = new Graba_Factura_DigitalMaestro();
        Graba_Factura_DigitalPago Graba_Factura_DigitalPago = new Graba_Factura_DigitalPago();
        Graba_Factura_NotaMaestro Graba_Factura_NotaMaestro = new Graba_Factura_NotaMaestro();
        Cancelacion_Factura_CFDMaestro Cancelacion_Factura_CFDMaestro = new Cancelacion_Factura_CFDMaestro();
        Graba_Factura_Digital Graba_Factura_Digital = new Graba_Factura_Digital();
        Graba_Factura_Nota Graba_Factura_Nota = new Graba_Factura_Nota();
        Graba_Factura_DigitalGlobal Graba_Factura_DigitalGlobal = new Graba_Factura_DigitalGlobal();
        Graba_Factura_DigitalMaestro_Refacturacion Graba_Factura_DigitalMaestro_Refacturacion = new Graba_Factura_DigitalMaestro_Refacturacion();
        Graba_Factura_DigitalMaestro_Viejo Graba_Factura_DigitalMaestro_Viejo = new Graba_Factura_DigitalMaestro_Viejo();
        //Alta_Datosfiscales
        /// <summary>
        /// Graba factura fiscal de Facturación Corporativa
        /// </summary>
        /// <param name="oClv_Factura"></param>
        /// <param name="oIden"></param>
        /// <returns></returns>
        public ResultEntity GetGraba_Factura_DigitalMaestrotvzac(long? oClv_Factura, long? oIden)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_DigitalMaestro.GetGraba_Factura_DigitalMaestrotvzac(oClv_Factura, oIden);
            }            
        }

        public ResultEntity GetGraba_Factura_DigitalMaestro_Refacturacion(long? oClv_Factura, long? Clv_FacturaRefacturacion)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_DigitalMaestro_Refacturacion.GetGraba_Factura_DigitalMaestro_Refacturacion(oClv_Factura, Clv_FacturaRefacturacion);
            }
        }

        /// <summary>
        /// Graba factura fiscal de pago de Facturación Corporativa
        /// </summary>
        /// <param name="oClv_Factura"></param>
        /// <param name="oIden"></param>
        /// <returns></returns>
        public ResultEntity GetGraba_Factura_DigitalPago(long? oClv_Factura, long? oIden)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_DigitalPago.GetGraba_Factura_DigitalPago(oClv_Factura, oIden);
            }
        }

        /// <summary>
        /// Imprime la factura fiscal de Facturación Corporativa
        /// </summary>
        /// <param name="oClv_Factura"></param>
        /// <returns></returns>
        public ResultEntity GetImprimeFacturaFiscal(long? oClv_Factura)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_DigitalMaestro.GetImprimeFacturaFiscal(oClv_Factura);
            }
        }

        /// <summary>
        /// Envia factura de Facturación Corporativa
        /// </summary>
        /// <param name="oClv_Factura"></param>
        /// <returns></returns>
        public ResultEntity GetEnviaFacturaFiscal(long? oClv_Factura)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_DigitalMaestro.GetEnviaFacturaFiscal(oClv_Factura);
            }
        }

        

        /// <summary>
        /// Imprime la factura fiscal de un pago de Facturación Corporativa
        /// </summary>
        /// <param name="oClv_Factura"></param>
        /// <returns></returns>
        public ResultEntity GetImprimeFacturaFiscalpago(long? oClv_Factura)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_DigitalPago.GetImprimeFacturaFiscalpago(oClv_Factura);
            }
        }

        /// <summary>
        /// Imprime la factura fiscal de la nota de crédito
        /// </summary>
        /// <param name="oClv_Nota"></param>
        /// <returns></returns>
        public ResultEntity GetImprimeFacturaFiscalNotaMaestro(long? oClv_Nota)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_NotaMaestro.GetImprimeFacturaFiscalNotaMaestro(oClv_Nota);
            }
        }

        public ResultEntity GetEnviaNotaCreditoMaestro(long? oClv_Factura)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_NotaMaestro.GetEnviaNotaCreditoMaestro(oClv_Factura);
            }
        }

        /// <summary>
        /// Envia por correo factura de pago de Facturacion Corporativa
        /// </summary>
        /// <param name="oClv_Factura"></param>
        /// <returns></returns>
        public ResultEntity GetEnviaFacturaFiscalpago(long? oClv_Factura)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_DigitalPago.GetEnviaFacturaFiscalpago(oClv_Factura);
            }
        }

        /// <summary>
        /// Graba factura de nota de crédito de maestro
        /// </summary>
        /// <param name="oClv_Factura"></param>
        /// <returns></returns>
        public ResultEntity GetGraba_Factura_NotaMaestro(long? oClv_Factura)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_NotaMaestro.GetGraba_Factura_NotaMaestro(oClv_Factura,0);
            }
        }

        /// <summary>
        /// Cancela factura maestro
        /// </summary>
        /// <param name="oClv_FacturaCFD"></param>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public  ResultEntity GetCancelacion_Factura_CFDMaestro(long? oClv_FacturaCFD, string tipo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Cancelacion_Factura_CFDMaestro.GetCancelacion_Factura_CFDMaestro(oClv_FacturaCFD, tipo);
            }
        }

        /// <summary>
        /// Graba la factura de una nota de facturación
        /// </summary>
        /// <param name="oClv_Factura"></param>
        /// <returns></returns>
        public ResultEntity GetGraba_Factura_Nota(long? oClv_Factura)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_Nota.GetGraba_Factura_Nota(oClv_Factura, 0);
            }
        }

        /// <summary>
        /// Graba la factura fiscal de un ticket de facturacion
        /// </summary>
        /// <param name="oClv_Factura"></param>
        /// <returns></returns>
        public ResultEntity GetGraba_Factura_Digital(long? oClv_Factura)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_Digital.GetGraba_Factura_Digital(oClv_Factura);
            }
        }

        /// <summary>
        /// Imprime la factura fiscal de ticket o nota de Facturación Softv
        /// </summary>
        /// <param name="oClv_Factura"></param>
        /// <returns></returns>
        public ResultEntity GetImprimeFacturaFiscalFac(long? oClv_Factura, string Tipo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_Digital.GetImprimeFacturaFiscal(oClv_Factura, Tipo);
            }
        }

        /// <summary>
        /// Envia factura de Facturación Corporativa
        /// </summary>
        /// <param name="oClv_Factura"></param>
        /// <returns></returns>
        public ResultEntity GetEnviaFacturaFiscalFac(long? oClv_Factura)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_Digital.GetEnviaFacturaFiscal(oClv_Factura);
            }
        }

        /// <summary>
        /// Graba la factura global de facturacion web
        /// </summary>
        /// <param name="oClv_Factura"></param>
        /// <returns></returns>
        public ResultEntity GetGraba_Factura_Digital_Global(long? oClv_Factura)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_DigitalGlobal.GetGraba_Factura_DigitalGlobal(oClv_Factura);
            }
        }

        /// <summary>
        /// Graba la factura global de facturacion web cuando se generan muchas de trancazo
        /// </summary>
        /// <param name="oClv_Factura"></param>
        /// <returns></returns>
        public List<ResultEntity> GetGraba_Factura_Digital_GlobalMuchas(List<long> ListoClv_Factura)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                List<ResultEntity> list = new List<ResultEntity>();
                foreach (int oClv_Factura in ListoClv_Factura)
                {
                    ResultEntity result = new ResultEntity();
                    result = Graba_Factura_DigitalGlobal.GetGraba_Factura_DigitalGlobal(oClv_Factura);
                    list.Add(result);
                }
                return list;
            }
        }

        /// <summary>
        /// Graba la factura maestro del viejo de escritorio
        /// </summary>
        /// <param name="oClv_Factura"></param>
        /// <returns></returns>
        public ResultEntity GetGraba_Factura_DigitalMaestro_Viejo(long? oClv_Factura)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                return Graba_Factura_DigitalMaestro_Viejo.GetGraba_Factura_DigitalMaestro_Viejo(oClv_Factura);      
            }
        }

    }
}
