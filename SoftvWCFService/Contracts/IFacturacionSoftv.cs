﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using Softv.Entities;
using softv.Entities;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IFacturacionSoftv
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGraba_Factura_DigitalMaestrotvzac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetGraba_Factura_DigitalMaestrotvzac(long? oClv_Factura, long? oIden);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGraba_Factura_DigitalPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetGraba_Factura_DigitalPago(long? oClv_Factura, long? oIden);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetImprimeFacturaFiscal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetImprimeFacturaFiscal(long? oClv_Factura);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetImprimeFacturaFiscalNotaMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetImprimeFacturaFiscalNotaMaestro(long? oClv_Nota);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEnviaFacturaFiscal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetEnviaFacturaFiscal(long? oClv_Factura);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetImprimeFacturaFiscalpago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetImprimeFacturaFiscalpago(long? oClv_Factura);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEnviaFacturaFiscalpago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetEnviaFacturaFiscalpago(long? oClv_Factura);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGraba_Factura_NotaMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetGraba_Factura_NotaMaestro(long? oClv_Factura);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCancelacion_Factura_CFDMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetCancelacion_Factura_CFDMaestro(long? oClv_FacturaCFD, string tipo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGraba_Factura_Digital", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetGraba_Factura_Digital(long? oClv_Factura);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGraba_Factura_Nota", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetGraba_Factura_Nota(long? oClv_Factura);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetImprimeFacturaFiscalFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetImprimeFacturaFiscalFac(long? oClv_Factura, string Tipo);


        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetImprimeFacturaFiscalNotaMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //ResultEntity GetImprimeFacturaFiscalNotaMaestro(long? oClv_Nota);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEnviaFacturaFiscalFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetEnviaFacturaFiscalFac(long? oClv_Factura);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGraba_Factura_Digital_Global", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetGraba_Factura_Digital_Global(long? oClv_Factura);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEnviaNotaCreditoMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetEnviaNotaCreditoMaestro(long? oClv_Factura);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGraba_Factura_Digital_GlobalMuchas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ResultEntity> GetGraba_Factura_Digital_GlobalMuchas(List<long> ListoClv_Factura);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGraba_Factura_DigitalMaestro_Refacturacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetGraba_Factura_DigitalMaestro_Refacturacion(long? oClv_Factura, long? Clv_FacturaRefacturacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGraba_Factura_DigitalMaestro_Viejo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetGraba_Factura_DigitalMaestro_Viejo(long? oClv_Factura);
    }
}

