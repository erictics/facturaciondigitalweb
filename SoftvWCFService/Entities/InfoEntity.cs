﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;


namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class InfoEntity
    {
        [DataMember]
        public int ? status { get; set; }

        [DataMember]
        public string message { get; set; }
    }

    [DataContract]
    [Serializable]
    public class companiaFacturaEntity
    {
        [DataMember]
        public long ? Clv_SessionMaestro { get; set; }

        [DataMember]
        public string ID_Compania_Mizart { get; set; }

        [DataMember]
        public string ID_Sucursal_Mizart { get; set; }

        [DataMember]
        public string ID_Compania_almacen { get; set; }
    }


    [DataContract]
    [Serializable]
    public class RelFacturasCFDMaestroEntity
    {
        public long? Clv_Factura { get; set; }

        public long? Clv_FacturaCFD { get; set; }

        public string Serie { get; set; }

        public string tipo { get; set; }

    }
}
