﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace softv.Entities
{
    [DataContract]
    [Serializable]
    public class ResultEntity
    {
        [DataMember]
        public long ? IdResult { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string urlReporte { get; set; }
    }
}