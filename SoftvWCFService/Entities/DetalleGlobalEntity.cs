﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class DetalleGlobalEntity
    {
        [DataMember]
        public decimal mIva { get; set; }


        [DataMember]
        public decimal mipes { get; set; }


        [DataMember]
        public decimal msubtotal { get; set; }


        [DataMember]
        public string mdetalle { get; set; }

    }
}