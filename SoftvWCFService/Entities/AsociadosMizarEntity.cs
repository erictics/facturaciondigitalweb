﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class AsociadosMizarEntity
    {
        [DataMember]
        public string oRazonSocial { get; set; }
        [DataMember]
        public string oRFC { get; set; }
        [DataMember]
        public string oCalle { get; set; }
        [DataMember]
        public string oClaveAsociado { get; set; }
        [DataMember]
        public string oCodigoPostal { get; set; }
        [DataMember]
        public string oColonia { get; set; }
        [DataMember]
        public string oEMail { get; set; }
        [DataMember]
        public string oEntreCalles { get; set; }
        [DataMember]
        public string oEstado { get; set; }
        [DataMember]
        public string oIdAsociado { get; set; }
        [DataMember]
        public string oLocalidad { get; set; }
        [DataMember]
        public string oMunicipio { get; set; }
        [DataMember]
        public string oNumeroExterior { get; set; }
        [DataMember]
        public string oNumeroInterior { get; set; }
        [DataMember]
        public string oPais { get; set; }
        [DataMember]
        public string oTel { get; set; }
        [DataMember]
        public string oFax { get; set; }
        [DataMember]
        public string oReferencia { get; set; }
        [DataMember]
        public string oContrato { get; set; }
        [DataMember]
        public string ometodo_de_pago { get; set; }


    }
}